package cpe.projet.erudilyon.wsquestions;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ContextConfiguration(classes = ErudiLyonWsQuestionsApplication.class)
@DataJpaTest
class ErudiLyonWsQuestionsApplicationTests {

    @PersistenceContext
    private EntityManager entityManager;

    @BeforeAll
    public static void setup() {
        // entityManager.persist(null);
        // entityManager.flush();
    }

    @Test
    public void test1() {

    }

}
