package cpe.projet.erudilyon.wsquestions.Repository;

import cpe.projet.erudilyon.wsquestions.Model.QuestionModel;
import org.springframework.data.repository.CrudRepository;

public interface QuestionRepository extends CrudRepository<QuestionModel, Integer> {


}
