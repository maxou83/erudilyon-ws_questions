package cpe.projet.erudilyon.wsquestions.Repository;

import cpe.projet.erudilyon.wsquestions.Model.QuestionnaireModel;
import org.springframework.data.repository.CrudRepository;

public interface QuestionnnaireRepository extends CrudRepository<QuestionnaireModel, Integer> {
}
