package cpe.projet.erudilyon.wsquestions.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

@ApiModel(description = "Représentation d'une question")
@Entity
@Table(name = "T_E_QUESTION_QUESTION")
@Data
public class QuestionModel
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "QUESTION_ID")
    @ApiModelProperty(value = "Identifiant technique d'une question", required = false)
    private Integer id;

    @Column(name = "QUESTION_CATEGOTY_ID", nullable = false)
    @ApiModelProperty(value = "ID de la catégorie de la question", required = true)
    private Integer categoryId;

    @Column(name = "QUESTION_TEXTE", nullable = false)
    @ApiModelProperty(value = "Texte de la question", required = true)
    private String questionTexte;

    @Column(name = "QUESTION_REP_A", nullable = false)
    @ApiModelProperty(value = "Réponse A à la question", required = true)
    private String reponseA;

    @Column(name = "QUESTION_REP_B", nullable = false)
    @ApiModelProperty(value = "Réponse B à la question", required = true)
    private String reponseB;

    @Column(name = "QUESTION_REP_C")
    @ApiModelProperty(value = "Réponse C à la question")
    private String reponseC;

    @Column(name = "QUESTION_REP_D")
    @ApiModelProperty(value = "Réponse D à la question")
    private String reponseD;

    @Column(name = "QUESTION_BONNE_REP", nullable = false)
    @ApiModelProperty(value = "Bonne réponse à la question", required = true)
    private String bonneReponse;

    @Column(name = "QUESTION_STATUT", nullable = false)
    @ApiModelProperty(value = "Statut de la question (actif ou inactif)", required = true)
    private Boolean status;

}
