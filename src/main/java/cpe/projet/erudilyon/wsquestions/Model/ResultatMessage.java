package cpe.projet.erudilyon.wsquestions.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@ApiModel(description = "Réponse validation d'un questionnaire")
public class ResultatMessage {

    @ApiModelProperty(value = "ID du questionnaire", required = true)
    private Integer id;

    @ApiModelProperty(value = "ID de l'utilisateur", required = true)
    private Integer userId;

    @ApiModelProperty(value = "Liste des réponses des questions du questionnaire", required = true)
    private List<ReponseModel> questions = new ArrayList<>();

}
