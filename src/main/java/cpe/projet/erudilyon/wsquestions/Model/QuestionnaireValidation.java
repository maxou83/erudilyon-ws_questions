package cpe.projet.erudilyon.wsquestions.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@ApiModel(description = "Validation d'un questionnaire")
public class QuestionnaireValidation {

    @ApiModelProperty(value = "Gain obtenu")
    private BigDecimal gain;

    @ApiModelProperty(value = "Information résultat")
    private String result;

}
