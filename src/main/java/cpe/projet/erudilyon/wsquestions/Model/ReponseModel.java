package cpe.projet.erudilyon.wsquestions.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "Réponse d'une question d'un questionnaire à faire valider")
public class ReponseModel {

    @ApiModelProperty(value = "ID de la question", required = true)
    private Integer id;

    @ApiModelProperty(value = "Réponse de l'utilisateur", required = true)
    private String reponse;

}
