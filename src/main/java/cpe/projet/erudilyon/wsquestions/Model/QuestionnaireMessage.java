package cpe.projet.erudilyon.wsquestions.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@ApiModel(description = "Représentation d'un questionnaire")
public class QuestionnaireMessage {

    @ApiModelProperty(value = "ID du questionnaire", required = true)
    private Integer id;

    @ApiModelProperty(value = "Date de génération du questionnaire", required = true)
    private Date date;

    @ApiModelProperty(value = "ID de la catégorie du questionnaire", required = true)
    private Integer categoryId;

    @ApiModelProperty(value = "Liste des id des questions du questionnaire", required = true)
    private List<QuestionModel> questions = new ArrayList<>();
}
