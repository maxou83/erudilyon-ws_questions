package cpe.projet.erudilyon.wsquestions.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "T_E_QUESTIONNAIRE_QUESTIONNAIRE")
@Data
public class QuestionnaireModel
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "QUESTIONNAIRE_ID", nullable = false)
    private Integer id;

    @Column(name = "QUESTIONNAIRE_REPONDU")
    private Boolean repondu;

    @Column(name = "QUESTIONNAIRE_DATE", nullable = false)
    private Date date;

    @Column(name = "QUESTIONNAIRE_USER_ID")
    private Integer userId;

    @Column(name = "QUESTIONNAIRE_CATEGOTY_ID", nullable = false)
    private Integer categoryId;

    @ElementCollection
    @Column(name = "QUESTIONNAIRE_QUESTION_ID_LIST", nullable = false)
    private List<Integer> questionsId;
}
