package cpe.projet.erudilyon.wsquestions.Service;

import Model.ActivityUserTransactionDTO;
import Model.UserDTO;
import cpe.projet.erudilyon.wsquestions.Model.*;
import cpe.projet.erudilyon.wsquestions.Repository.QuestionRepository;
import cpe.projet.erudilyon.wsquestions.Repository.QuestionnnaireRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class QuestionnaireService {

    private QuestionRepository repository;
    private QuestionnnaireRepository questionnnaireRepository;
    private JdbcTemplate jdbcTemplate;

    public QuestionnaireService(QuestionRepository repository, QuestionnnaireRepository questionnnaireRepository, JdbcTemplate jdbcTemplate) {
        this.repository = repository;
        this.questionnnaireRepository = questionnnaireRepository;
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Permet de générer un questionnaire
     *
     * @param categoryId la catégorie souhaitée
     * @return le questionnaire
     */
    public QuestionnaireMessage generateQuestionnaire(Integer categoryId) {

        // TODO contrôle pas de 404 pour la catégorie

        QuestionnaireModel model = new QuestionnaireModel();
        model.setCategoryId(categoryId);
        model.setDate(new Date());
        model.setRepondu(false);

        List<Integer> list = jdbcTemplate.queryForList("select QUESTION_ID from T_E_QUESTION_QUESTION where QUESTION_CATEGOTY_ID = ? " +
                " order by RANDOM() LIMIT 10 ", Integer.class, categoryId );

        model.setQuestionsId(list);

        model = questionnnaireRepository.save(model);

        QuestionnaireMessage message = new QuestionnaireMessage();
        message.setCategoryId(model.getCategoryId());
        message.setId(model.getId());
        message.setDate(model.getDate());

        List<QuestionModel> listQuestion = new ArrayList<>();
        repository.findAllById(model.getQuestionsId()).forEach(listQuestion::add);
        message.setQuestions(listQuestion);

        return message;
    }

    /**
     * Permet de valider un questionnaire
     *
     * @param resultatMessage la saisie du questionnare
     * @return le résultat
     */
    public QuestionnaireValidation validate(ResultatMessage resultatMessage) {
        QuestionnaireModel model = questionnnaireRepository.findById(resultatMessage.getId()).orElse(null);

        if (model == null)
            return new QuestionnaireValidation(BigDecimal.ZERO, "KO");

        model.setUserId(resultatMessage.getUserId());
        model.setRepondu(true);

        BigDecimal gain = BigDecimal.ZERO;

        for (ReponseModel reponseModel : resultatMessage.getQuestions()) {
            QuestionModel questionModel = repository.findById(reponseModel.getId()).orElse(null);
            if (questionModel != null)
            {
                if (StringUtils.equals(questionModel.getBonneReponse(), reponseModel.getReponse()))
                    gain = gain.add(BigDecimal.valueOf(2));
            }
        }

        updateUserSolde(resultatMessage.getUserId(), gain);

        return new QuestionnaireValidation(gain, "OK");

    }

    /**
     * Permet de créditer l'utilsateur après validation de son questionnaire
     *
     * @param userId
     * @param gain
     * @return
     */
    public UserDTO updateUserSolde(Integer userId, BigDecimal gain) {

        RestTemplate restTemplate = new RestTemplateBuilder().build();
        String url =  "http://asi-projet-cpe.northeurope.cloudapp.azure.com" + ":8084/addSolde";

        ActivityUserTransactionDTO transaction = new ActivityUserTransactionDTO();
        transaction.setUserId(userId);
        transaction.setAmount(gain);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBasicAuth("admin", "admin");

        HttpEntity<ActivityUserTransactionDTO> entity = new HttpEntity(transaction, headers);

        return restTemplate.postForObject(url, entity, UserDTO.class);

    }
}
