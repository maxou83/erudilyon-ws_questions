package cpe.projet.erudilyon.wsquestions.Service;

import cpe.projet.erudilyon.wsquestions.Model.QuestionModel;
import cpe.projet.erudilyon.wsquestions.Repository.QuestionRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class QuestionService {

    private QuestionRepository repository;

    public QuestionService(QuestionRepository repository) {
        this.repository = repository;
    }

    public QuestionModel add(QuestionModel model) {
        return repository.save(model);
    }

    public List<QuestionModel> all() {
        List<QuestionModel> questionModelList = new ArrayList<>();

        repository.findAll().forEach(questionModelList::add);

        return questionModelList;
    }

    public QuestionModel get(Integer id)
    {
        return repository.findById(id).orElse(null);
    }
}
