package cpe.projet.erudilyon.wsquestions.Controller;

import cpe.projet.erudilyon.wsquestions.Model.*;
import cpe.projet.erudilyon.wsquestions.Service.QuestionService;
import cpe.projet.erudilyon.wsquestions.Service.QuestionnaireService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@Api(value = "API de Gestion des questionnaire")
public class QuestionnaireController {

    private QuestionService questionService;
    private QuestionnaireService questionnaireService;

    public QuestionnaireController(QuestionService questionService, QuestionnaireService questionnaireService) {
        this.questionService = questionService;
        this.questionnaireService = questionnaireService;
    }

    @ApiOperation(value = "Permet de générer un questionnaire", response = QuestionnaireMessage.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PostAuthorize("hasAnyRole('USER','ADMIN','PARTNER')")
    @PostMapping("questionnaire/generate/{categoryId}")
    public QuestionnaireMessage generateQuestionnaire(@PathVariable Integer categoryId) {
        return questionnaireService.generateQuestionnaire(categoryId);
    }

    @ApiOperation(value = "Permet de valider un questionnaire", response = QuestionnaireValidation.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PostAuthorize("hasAnyRole('USER','ADMIN','PARTNER')")
    @PostMapping("/questionnaire/validate")
    public QuestionnaireValidation validate(@RequestBody ResultatMessage resultatMessage)
    {
        return questionnaireService.validate(resultatMessage);
    }



}
