package cpe.projet.erudilyon.wsquestions.Controller;

import cpe.projet.erudilyon.wsquestions.Model.QuestionModel;
import cpe.projet.erudilyon.wsquestions.Service.QuestionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@Api(value = "API de Gestion des questions")
public class QuestionsController {

    private QuestionService questionService;

    public QuestionsController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @ApiOperation(value = "Permet de controler la disponibilité du service", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @GetMapping("/up")
    public String health() {
        return "UP!";
    }

    @ApiOperation(value = "Permet d'ajouter une question", response = QuestionModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PostAuthorize("hasAnyRole('ADMIN','PARTNER')")
    @PostMapping("questions/add")
    public QuestionModel addQuestion(@RequestBody QuestionModel model) {
        return questionService.add(model);
    }

    @ApiOperation(value = "Permet d'avoir toutes les questions", response = QuestionModel.class, responseContainer="List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PostAuthorize("hasAnyRole('ADMIN','PARTNER')")
    @GetMapping("questions/all")
    public List<QuestionModel> getAllQuestions() {
        return questionService.all();
    }

    @ApiOperation(value = "Permet de récupérer une question par son ID", response = QuestionModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PostAuthorize("hasAnyRole('ADMIN','PARTNER')")
    @GetMapping("question/{id}")
    public QuestionModel getQuestion(@PathVariable Integer id) {
        return questionService.get(id);
    }


}
